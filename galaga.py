import pygame
import sys
from clases.nave import Nave
from clases.disparo import Disparo
from clases.alien import Alien

pygame.init()

class Juego():

    alien = Alien()

    color_fondo = (25,20,150)
    ventana = pygame.display.set_mode([700,550]) #crear ventana
    
    personaje=Nave()
    
    reloj = pygame.time.Clock()
    disparos = pygame.sprite.Group()

    velocidad = 8

    
    pygame.display.set_caption("Portillo,Molina y Sanchez") 

    posicion_x = 0

    def cargar_imagenes(self):
        self.fondo = pygame.image.load("ASSETS/spritesheet/galaxia.jpg")
        self.fondo=pygame.transform.scale(self.fondo,(1000,700))

    def jugar(self):
        

        while True:     #Bucle de "Juego"
            ''' Esto significa que se van realizan 30
                actualizaciones del juego por segundo.
                Es necesario hacerlo en cada iteración
                por que si no se reinicia
            '''

            for event in pygame.event.get():    #Cuando ocurre un evento...
                
                #print("EVENTO: ", pygame.key.get_pressed())
                if event.type == pygame.QUIT:   #Si el evento es cerrar la ventana
                    pygame.quit()        #Se cierra pygame
                    sys.exit()           #Se cierra el programa
        
                if event.type == pygame.KEYDOWN:
                    #print("Apreto tecla: ", event.key)
                    if event.key == pygame.K_RIGHT:
                        pass #marciano.update("derecha")
                
                if event.type== pygame.KEYUP:
                    if pygame.key.get_pressed()[pygame.K_c]:
                        disparo = Disparo(self.personaje)

                        self.ventana.blit(disparo.image, disparo.rect)
                        disparo.update()

            self.ventana.blit(self.fondo, (0, 0))

            self.ventana.blit(self.personaje.image, self.personaje.rect)
            self.personaje.administrar_eventos(event)

            
            
            pygame.display.update()
            

            self.reloj.tick(30)


juego=Juego()
juego.cargar_imagenes()
juego.jugar()