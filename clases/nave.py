import pygame  
from clases.disparo import Disparo

class Nave(pygame.sprite.Sprite):
    spritesheet_x=None
    velocidad = 8
    frames_derecha =None
    frames_izquierda =None
    frames_abajo =None
    frames_arriba =None
    frames_width=32
    frames_height=48
    frame=0
    puntos = 0
    vida = 10

    def __init__(self):
        super().__init__()
        self.spritesheet_x=pygame.image.load ("ASSETS/spritesheet/g.jpg").convert_alpha()
        #imagen de inicio
        self.spritesheet_x.set_clip(pygame.Rect(237,2,35, 35))
        self.image=self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())
        self.image.set_colorkey( ( 0,0,0 ))
        self.frames_derecha =  {0:(160,0,self.frames_width,self.frames_height), 1:(192,0,self.frames_width,self.frames_height), 2:(224,0,self.frames_width,self.frames_height), 3:(256,0,self.frames_width,self.frames_height)}
        self.frames_izquierda = {0:(0,0,self.frames_width,self.frames_height)}    

        self.rect = self.image.get_rect()
        self.rect.x = 500
        self.rect.y = 350


    def devolver_frame(self, frame_set):
        self.frame += 1
        if self.frame >= len(frame_set):
            self.frame = 0
        return frame_set[self.frame]


    def clip(self, dict_sprite):
        self.spritesheet_x.set_clip(pygame.Rect(self.devolver_frame(dict_sprite)))

    def update(self, direccion):
        
        if direccion == "derecha":
            
            #self.clip(self.frames_derecha)
            if self.rect.x >= 1500:
                self.rect.x = 0
            else:
                self.rect.x += self.velocidad
        
        elif direccion == "izquierda":
            #self.clip(self.frames_izquierda)
            if self.rect.x <= 0:
                self.rect.x = 500
            self.rect.x -= self.velocidad
        
        elif direccion == "arriba":
            if self.rect.y > 350:
                self.rect.y -= self.velocidad

        elif direccion == "disparar":
            a=Disparo()
        
        elif direccion == "abajo":
            #self.clip(self.frames_izquierda)  cambia la imagen yendo para abajo
            if self.rect.y >= 510:
                self.rect.y += 0
            else:
                self.rect.y += self.velocidad
            """
            if self.rect.y <= 500:
                self.rect.y += self.velocidad
            """

        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())
        self.image.set_colorkey( ( 0,0,0 ))

    
    def administrar_eventos(self, evento):
        if evento.type == pygame.KEYDOWN:

            if evento.key == pygame.K_RIGHT:
          
                self.update("derecha")
            
            if evento.key == pygame.K_LEFT:
                self.update("izquierda")
            
            if evento.key == pygame.K_UP:
                self.update("arriba")
            
            if evento.key == pygame.K_DOWN:
                self.update("abajo")
            
            if evento.key == pygame.K_a:
                self.update("disparar")

 