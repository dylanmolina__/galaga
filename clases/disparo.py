import pygame

class Disparo(pygame.sprite.Sprite):
    frames_ancho=55
    frames_alto=25
    velocidad = 7

    def __init__(self, nave):
        super().__init__()
        self.spritesheet_x=pygame.image.load ("ASSETS/spritesheet/g.jpg").convert_alpha()
       
        self.spritesheet_x.set_clip(pygame.Rect(628,241,661-628, 275 - 241))
        self.image=self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())

        self.rect = self.image.get_rect()
        self.rect.x = nave.rect.centerx
        self.rect.y = nave.rect.centery

    
    def update(self):
        
        self.rect.y -= self.velocidad